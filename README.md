#Python binary file to hex text file

Simple script to convert binary files like jpegs to hex lists outputed as string in file

## Usage

Default result will be written in output.txt file

```bash
python3 binary-to-list.py input_binary_file.bin

# Text file will be writter in > output.txt
```

Second parameter will customize the output file

```bash
python3 binary-to-list.py input_binary_file.bin my_file_name.txt

# Text file will be writter in > my_file_name.txt
```

## Customizations

```python
# The output file customization
output_file = "./output.txt"

# Number of bytes per line in generated text file
bytes_per_line = 24

# Bytes string separator: here it will be 0x78, ...
byte_separator = ', '

# End of lines of each byte line
break_line_string = '\r\n'
```