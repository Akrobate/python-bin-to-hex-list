import sys

# Configurations
output_file = "./output.txt"
bytes_per_line = 24
byte_separator = ', '
break_line_string = '\r\n'

def fileToBytesStringList(file):
	file = open(file, "rb")
	string_hex_list = []
	byte = file.read(1)
	while(byte):
		byte_std = '0x' + format(int.from_bytes(byte, "big"), '02x')
		string_hex_list.append(byte_std)
		byte = file.read(1)
	return string_hex_list


def buildOutputString(string_hex_list, byte_separator, bytes_per_line, break_line_string):
	cnt = 0
	output_string = ""
	for byte_str in string_hex_list:
		output_string += byte_str + byte_separator
		cnt += 1
		if (cnt == bytes_per_line):
			output_string += break_line_string
			cnt = 0
	return output_string[:-2]


def saveFile(file, output_string):
	with open(file, "w") as text_file:
		text_file.write(output_string)


if __name__ == "__main__":
	args = sys.argv[1:]
	if (len(args) < 1):
		print("First parameter must be a file name")
	else:
		if (len(args) > 1):
			output_file = args[1]

		input_file = args[0]
		print("Processing file: " + input_file)
		string_hex_list = fileToBytesStringList(input_file)
		output_string = buildOutputString(string_hex_list, byte_separator, bytes_per_line, break_line_string)
		saveFile(output_file, output_string)
		print("Generated " + str(len(string_hex_list)) + " bytes")
		print("Output file file: " + output_file)

